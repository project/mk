# Macedonian translation of [ www.linux.net.mk ]
# Copyright (c) 2005 Арангел <ufo@users.ossm.org.mk>
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2005-04-15 17:59+0000\n"
"PO-Revision-Date: 2005-03-22 08:08-0600\n"
"Last-Translator: Арангел <ufo@users.ossm.org.mk>\n"
"Language-Team: Macedonian <ufo@users.ossm.org.mk>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: includes/locale.inc:26
msgid ""
"%locale language added. You can now import a translation. See the <a href=\"%"
"locale-help\">help screen</a> for more information."
msgstr ""

#: includes/locale.inc:29
#, fuzzy
msgid "%locale language added."
msgstr "Сите јазици"

#: includes/locale.inc:33
msgid "%language language (%locale) added."
msgstr ""

#: includes/locale.inc:43
msgid "Code"
msgstr ""

#: includes/locale.inc:43
msgid "English name"
msgstr "Англиско име"

#: includes/locale.inc:43
msgid "Translated"
msgstr "Преведени"

#: includes/locale.inc:72
#, fuzzy
msgid "From language list"
msgstr "јазици"

#: includes/locale.inc:73 ;704
msgid "Language name"
msgstr ""

#: includes/locale.inc:73
msgid ""
"Select your language here, or add it below, if you are unable to find it."
msgstr ""

#: includes/locale.inc:78
#, fuzzy
msgid "Custom language"
msgstr "јазици"

#: includes/locale.inc:79
#, fuzzy
msgid "Language code"
msgstr "Јазик"

#: includes/locale.inc:79
msgid ""
"Commonly this is an <a href=\"%iso-codes\">ISO 639 language code</a> with an "
"optional country code for regional variants. Examples include 'en', 'en-US' "
"and 'zh-cn'."
msgstr ""

#: includes/locale.inc:80
msgid "Language name in English"
msgstr ""

#: includes/locale.inc:80
msgid ""
"Name of the language. Will be available for translation in all languages."
msgstr ""

#: includes/locale.inc:101
#, fuzzy
msgid "Already added languages"
msgstr "додади јазик"

#: includes/locale.inc:102
msgid "Languages not yet added"
msgstr ""

#: includes/locale.inc:106
#, fuzzy
msgid "Language file"
msgstr "Јазик"

#: includes/locale.inc:106
msgid "A gettext Portable Object (.po) file."
msgstr ""

#: includes/locale.inc:107
msgid "Import into"
msgstr ""

#: includes/locale.inc:107
msgid ""
"Choose the language you want to add strings into. If you choose a language "
"which is not yet set up, then it will be added."
msgstr ""

#: includes/locale.inc:108
#, fuzzy
msgid "Mode"
msgstr "точка"

#: includes/locale.inc:108
msgid "Strings in the uploaded file replace existing ones, new ones are added"
msgstr ""

#: includes/locale.inc:108
msgid "Existing strings are kept, only new strings are added"
msgstr ""

#: includes/locale.inc:129
msgid "Unsupported language selected for import."
msgstr ""

#: includes/locale.inc:135
msgid "Translation file %filename broken: Could not be read."
msgstr ""

#: includes/locale.inc:157
msgid "Translation file %filename broken: No header."
msgstr ""

#: includes/locale.inc:259
msgid ""
"Translation successfully imported. %number translated strings added to "
"language, %update strings updated."
msgstr ""

#: includes/locale.inc:260
msgid ""
"Imported %file into %locale: %number new strings added and %update updated."
msgstr ""

#: includes/locale.inc:275
msgid "Translation import failed: file %filename cannot be read."
msgstr ""

#: includes/locale.inc:307
msgid "Translation file %filename broken: expected 'msgstr' in line %line."
msgstr ""

#: includes/locale.inc:313
msgid ""
"Translation file %filename broken: unexpected 'msgid_plural' in line %line."
msgstr ""

#: includes/locale.inc:319 ;337;349;357;371;380
msgid "Translation file %filename broken: syntax error in line %line."
msgstr ""

#: includes/locale.inc:331
msgid "Translation file %filename broken: unexpected 'msgid' in line %line."
msgstr ""

#: includes/locale.inc:345
msgid "Translation file %filename broken: unexpected 'msgstr[]' in line %line."
msgstr ""

#: includes/locale.inc:365
msgid "Translation file %filename broken: unexpected 'msgstr' in line %line."
msgstr ""

#: includes/locale.inc:393
msgid "Translation file %filename broken: unexpected string in line %line."
msgstr ""

#: includes/locale.inc:404
msgid ""
"Translation file %filename broken: unexpected end of file at line %line."
msgstr ""

#: includes/locale.inc:469
msgid "Translation file %filename broken: plural formula couldn't get parsed."
msgstr ""

#: includes/locale.inc:703
msgid "Export translation"
msgstr ""

#: includes/locale.inc:704
msgid ""
"Select the language you would like to export in gettext Portable Object (."
"po) format."
msgstr ""

#: includes/locale.inc:710
msgid "Export template"
msgstr ""

#: includes/locale.inc:711
msgid ""
"<p>Generate a gettext Portable Object Template (.pot) file with all the "
"interface strings from the Drupal locale database.</p>"
msgstr ""

#: includes/locale.inc:772
msgid "Exported %locale translation file: %filename."
msgstr ""

#: includes/locale.inc:793
msgid "Exported translation file: %filename."
msgstr ""

#: includes/locale.inc:908
#, fuzzy
msgid "Deleted string"
msgstr "избриши стринг"

#: includes/locale.inc:935
msgid "Saved string"
msgstr "Зачуван стринг"

#: includes/locale.inc:955
msgid "Original text"
msgstr "Оригинален текст"

#: includes/locale.inc:1046
msgid "String"
msgstr "Стринг"

#: includes/locale.inc:1046
msgid "Locales"
msgstr ""

#: includes/locale.inc:1088
msgid "Strings to search for"
msgstr "Стрингови за пребарување"

#: includes/locale.inc:1088
msgid "Leave blank to show all strings. The search is case sensitive."
msgstr ""

#: includes/locale.inc:1089
msgid "All languages"
msgstr "Сите јазици"

#: includes/locale.inc:1089
msgid "English (provided by Drupal)"
msgstr "Англиски (од „Drupal“)"

#: includes/locale.inc:1090
msgid "Search in"
msgstr "Барај во"

#: includes/locale.inc:1090
msgid "All strings in that language"
msgstr "Сите стрингови во тој јазик"

#: includes/locale.inc:1090
msgid "Only translated strings"
msgstr "Само преведени стрингови"

#: includes/locale.inc:1090
msgid "Only untranslated strings"
msgstr "Само непреведени стрингови"

#: includes/locale.inc:1093
msgid "Search strings"
msgstr "Пребарувај стрингови"

#: includes/locale.inc:1129
msgid "Afar"
msgstr ""

#: includes/locale.inc:1129
msgid "Abkhazian"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Avestan"
msgstr "Аватар"

#: includes/locale.inc:1129
msgid "Afrikaans"
msgstr ""

#: includes/locale.inc:1129
msgid "Akan"
msgstr ""

#: includes/locale.inc:1129
msgid "Amharic"
msgstr ""

#: includes/locale.inc:1129
msgid "Arabic"
msgstr ""

#: includes/locale.inc:1129
msgid "Assamese"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Avar"
msgstr "Аватар"

#: includes/locale.inc:1129
msgid "Aymara"
msgstr ""

#: includes/locale.inc:1129
msgid "Azerbaijani"
msgstr ""

#: includes/locale.inc:1129
msgid "Bashkir"
msgstr ""

#: includes/locale.inc:1129
msgid "Belarusian"
msgstr ""

#: includes/locale.inc:1129
msgid "Bulgarian"
msgstr ""

#: includes/locale.inc:1129
msgid "Bihari"
msgstr ""

#: includes/locale.inc:1129
msgid "Bislama"
msgstr ""

#: includes/locale.inc:1129
msgid "Bambara"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Bengali"
msgstr "вклучи"

#: includes/locale.inc:1129
#, fuzzy
msgid "Tibetan"
msgstr "Време"

#: includes/locale.inc:1129
msgid "Breton"
msgstr ""

#: includes/locale.inc:1129
msgid "Bosnian"
msgstr ""

#: includes/locale.inc:1129
msgid "Catalan"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Chechen"
msgstr "Избор %n"

#: includes/locale.inc:1129
msgid "Chamorro"
msgstr ""

#: includes/locale.inc:1129
msgid "Corsican"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Cree"
msgstr "Отворена"

#: includes/locale.inc:1129
msgid "Czech"
msgstr ""

#: includes/locale.inc:1129
msgid "Old Slavonic"
msgstr ""

#: includes/locale.inc:1129
msgid "Welsh"
msgstr ""

#: includes/locale.inc:1129
msgid "Welch"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Danish"
msgstr "Англиски"

#: includes/locale.inc:1129
msgid "German"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Maldivian"
msgstr "Македонски"

#: includes/locale.inc:1129
msgid "Bhutani"
msgstr ""

#: includes/locale.inc:1129
msgid "Ewe"
msgstr ""

#: includes/locale.inc:1129
msgid "Greek"
msgstr ""

#: includes/locale.inc:1129
msgid "English"
msgstr "Англиски"

#: includes/locale.inc:1129
#, fuzzy
msgid "Esperanto"
msgstr "Операции"

#: includes/locale.inc:1129
msgid "Spanish"
msgstr ""

#: includes/locale.inc:1129
msgid "Estonian"
msgstr ""

#: includes/locale.inc:1129
msgid "Basque"
msgstr ""

#: includes/locale.inc:1129
msgid "Persian"
msgstr ""

#: includes/locale.inc:1129
msgid "Fulah"
msgstr ""

#: includes/locale.inc:1129
msgid "Finnish"
msgstr ""

#: includes/locale.inc:1129
msgid "Fiji"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Faeroese"
msgstr "ресетирај"

#: includes/locale.inc:1129
msgid "French"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Frisian"
msgstr "Петок"

#: includes/locale.inc:1129
msgid "Irish"
msgstr ""

#: includes/locale.inc:1129
msgid "Scots Gaelic"
msgstr ""

#: includes/locale.inc:1129
msgid "Galician"
msgstr ""

#: includes/locale.inc:1129
msgid "Guarani"
msgstr ""

#: includes/locale.inc:1129
msgid "Gujarati"
msgstr ""

#: includes/locale.inc:1129
msgid "Manx"
msgstr ""

#: includes/locale.inc:1129
msgid "Hausa"
msgstr ""

#: includes/locale.inc:1129
msgid "Hebrew"
msgstr ""

#: includes/locale.inc:1129
msgid "Hindi"
msgstr ""

#: includes/locale.inc:1129
msgid "Hiri Motu"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Croatian"
msgstr "Локација"

#: includes/locale.inc:1129
msgid "Hungarian"
msgstr ""

#: includes/locale.inc:1129
msgid "Armenian"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Herero"
msgstr "грешка"

#: includes/locale.inc:1129
msgid "Interlingua"
msgstr ""

#: includes/locale.inc:1129
msgid "Indonesian"
msgstr ""

#: includes/locale.inc:1129
msgid "Interlingue"
msgstr ""

#: includes/locale.inc:1129
msgid "Igbo"
msgstr ""

#: includes/locale.inc:1129
msgid "Inupiak"
msgstr ""

#: includes/locale.inc:1129
msgid "Icelandic"
msgstr ""

#: includes/locale.inc:1129
msgid "Italian"
msgstr ""

#: includes/locale.inc:1129
msgid "Inuktitut"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Japanese"
msgstr "страници"

#: includes/locale.inc:1129
msgid "Javanese"
msgstr ""

#: includes/locale.inc:1129
msgid "Georgian"
msgstr ""

#: includes/locale.inc:1129
msgid "Kongo"
msgstr ""

#: includes/locale.inc:1129
msgid "Kikuyu"
msgstr ""

#: includes/locale.inc:1129
msgid "Kwanyama"
msgstr ""

#: includes/locale.inc:1129
msgid "Kazakh"
msgstr ""

#: includes/locale.inc:1129
msgid "Greenlandic"
msgstr ""

#: includes/locale.inc:1129
msgid "Cambodian"
msgstr ""

#: includes/locale.inc:1129
msgid "Kannada"
msgstr ""

#: includes/locale.inc:1129
msgid "Korean"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Kanuri"
msgstr "Јануари"

#: includes/locale.inc:1129
msgid "Kashmiri"
msgstr ""

#: includes/locale.inc:1129
msgid "Kurdish"
msgstr ""

#: includes/locale.inc:1129
msgid "Komi"
msgstr ""

#: includes/locale.inc:1129
msgid "Cornish"
msgstr ""

#: includes/locale.inc:1129
msgid "Kirghiz"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Latin"
msgstr "Локација"

#: includes/locale.inc:1129
msgid "Luxembourgish"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Luganda"
msgstr "Недела"

#: includes/locale.inc:1129
msgid "Lingala"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Laothian"
msgstr "Локација"

#: includes/locale.inc:1129
msgid "Lithuanian"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Latvian"
msgstr "Локација"

#: includes/locale.inc:1129
msgid "Malagasy"
msgstr ""

#: includes/locale.inc:1129
msgid "Marshallese"
msgstr ""

#: includes/locale.inc:1129
msgid "Maori"
msgstr ""

#: includes/locale.inc:1129
msgid "Macedonian"
msgstr "Македонски"

#: includes/locale.inc:1129
msgid "Malayalam"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Mongolian"
msgstr "Македонски"

#: includes/locale.inc:1129
msgid "Moldavian"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Marathi"
msgstr "Март"

#: includes/locale.inc:1129
#, fuzzy
msgid "Malay"
msgstr "Мај"

#: includes/locale.inc:1129
msgid "Maltese"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Burmese"
msgstr "ресетирај"

#: includes/locale.inc:1129
msgid "Nauru"
msgstr ""

#: includes/locale.inc:1129
msgid "North Ndebele"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Nepali"
msgstr "Реплики"

#: includes/locale.inc:1129
msgid "Ndonga"
msgstr ""

#: includes/locale.inc:1129
msgid "Dutch"
msgstr ""

#: includes/locale.inc:1129
msgid "Norwegian"
msgstr ""

#: includes/locale.inc:1129
msgid "South Ndebele"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Navajo"
msgstr "Навигација"

#: includes/locale.inc:1129
#, fuzzy
msgid "Chichewa"
msgstr "Избори"

#: includes/locale.inc:1129
msgid "Occitan"
msgstr ""

#: includes/locale.inc:1129
msgid "Oromo"
msgstr ""

#: includes/locale.inc:1129
msgid "Oriya"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Ossetian"
msgstr "подесувања"

#: includes/locale.inc:1129
msgid "Punjabi"
msgstr ""

#: includes/locale.inc:1129
msgid "Pali"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Polish"
msgstr "Објавено"

#: includes/locale.inc:1129
msgid "Pashto"
msgstr ""

#: includes/locale.inc:1129
msgid "Portuguese"
msgstr ""

#: includes/locale.inc:1129
msgid "Quechua"
msgstr ""

#: includes/locale.inc:1129
msgid "Rhaeto-Romance"
msgstr ""

#: includes/locale.inc:1129
msgid "Kirundi"
msgstr ""

#: includes/locale.inc:1129
msgid "Romanian"
msgstr ""

#: includes/locale.inc:1129
msgid "Russian"
msgstr ""

#: includes/locale.inc:1129
msgid "Kinyarwanda"
msgstr ""

#: includes/locale.inc:1129
msgid "Sanskrit"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Sardinian"
msgstr "Македонски"

#: includes/locale.inc:1129
msgid "Sindhi"
msgstr ""

#: includes/locale.inc:1129
msgid "Northern Sami"
msgstr ""

#: includes/locale.inc:1129
msgid "Sango"
msgstr ""

#: includes/locale.inc:1129
msgid "Serbo-Croatian"
msgstr ""

#: includes/locale.inc:1129
msgid "Singhalese"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Slovak"
msgstr "Слоган"

#: includes/locale.inc:1129
#, fuzzy
msgid "Slovenian"
msgstr "Слоган"

#: includes/locale.inc:1129
#, fuzzy
msgid "Samoan"
msgstr "Слоган"

#: includes/locale.inc:1129
#, fuzzy
msgid "Shona"
msgstr "Покажи"

#: includes/locale.inc:1129
msgid "Somali"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Albanian"
msgstr "Алијас"

#: includes/locale.inc:1129
#, fuzzy
msgid "Serbian"
msgstr "Барај во"

#: includes/locale.inc:1129
msgid "Siswati"
msgstr ""

#: includes/locale.inc:1129
msgid "Sesotho"
msgstr ""

#: includes/locale.inc:1129
msgid "Sudanese"
msgstr ""

#: includes/locale.inc:1129
msgid "Swedish"
msgstr ""

#: includes/locale.inc:1129
msgid "Swahili"
msgstr ""

#: includes/locale.inc:1129
msgid "Tamil"
msgstr ""

#: includes/locale.inc:1129
msgid "Telugu"
msgstr ""

#: includes/locale.inc:1129
msgid "Tajik"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Thai"
msgstr "Чет"

#: includes/locale.inc:1129
msgid "Tigrinya"
msgstr ""

#: includes/locale.inc:1129
msgid "Turkmen"
msgstr ""

#: includes/locale.inc:1129
msgid "Tagalog"
msgstr ""

#: includes/locale.inc:1129
msgid "Setswana"
msgstr ""

#: includes/locale.inc:1129
msgid "Tonga"
msgstr ""

#: includes/locale.inc:1129
msgid "Turkish"
msgstr ""

#: includes/locale.inc:1129
msgid "Tsonga"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Tatar"
msgstr "Аватар"

#: includes/locale.inc:1129
msgid "Twi"
msgstr ""

#: includes/locale.inc:1129
msgid "Tahitian"
msgstr ""

#: includes/locale.inc:1129
msgid "Uighur"
msgstr ""

#: includes/locale.inc:1129
msgid "Ukrainian"
msgstr ""

#: includes/locale.inc:1129
msgid "Urdu"
msgstr ""

#: includes/locale.inc:1129
msgid "Uzbek"
msgstr ""

#: includes/locale.inc:1129
msgid "Venda"
msgstr ""

#: includes/locale.inc:1129
#, fuzzy
msgid "Vietnamese"
msgstr "имиња на хостови"

#: includes/locale.inc:1129
msgid "Volapük"
msgstr ""

#: includes/locale.inc:1129
msgid "Wolof"
msgstr ""

#: includes/locale.inc:1129
msgid "Xhosa"
msgstr ""

#: includes/locale.inc:1129
msgid "Yiddish"
msgstr ""

#: includes/locale.inc:1129
msgid "Yoruba"
msgstr ""

#: includes/locale.inc:1129
msgid "Zhuang"
msgstr ""

#: includes/locale.inc:1129
msgid "Chinese, Simplified"
msgstr ""

#: includes/locale.inc:1129
msgid "Chinese, Traditional"
msgstr ""

#: includes/locale.inc:1129
msgid "Zulu"
msgstr ""
